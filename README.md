# Symmetric Encryption with Custom Key (SEWCK)

SEWCK is a PHP script that uses symmetric encryption with a custom key to encrypt and decrypt text data. The script uses the AES-256 encryption algorithm in CBC mode to ensure secure encryption of the data.

## Features

- Encrypt and decrypt text using AES-256 encryption
- Support for multi-line input during encryption
- Single-line input for decryption
- Option to save encrypted data to a file
- File input/output support for both encryption and decryption
- Interactive menu-driven interface

## Installation

1. Clone or download the repository to your local machine.
2. Ensure that you have PHP installed on your machine. You can check if PHP is installed by running the command `php -v` in your terminal or command prompt.
3. Open a terminal or command prompt and navigate to the directory where you cloned or downloaded the repository.

## Usage

Run the script using the command:

```
php script.php
```

When you run the script, you will be presented with a menu:

```
What do you want to do?
1. Encrypt
2. Decrypt
3. Exit
Enter your choice (1, 2, or 3):
```

### Encryption

If you choose to encrypt data:

1. You will be prompted to enter the text to encrypt. You can enter multiple lines of text. Type 'EOF' on a new line and press Enter when you're finished.
2. Enter the encryption key when prompted.
3. The script will output the encrypted data.
4. You will be asked if you want to save the encrypted data to a file.

### Decryption

If you choose to decrypt data:

1. You will be asked if you want to input the encrypted data directly or from a file.
2. If inputting directly, paste or type the entire encrypted string in a single line.
3. If using a file, enter the filename when prompted.
4. Enter the decryption key when prompted.
5. The script will output the decrypted data.

### Exiting the Program

Choose option 3 from the main menu to exit the program.

## Security Considerations

- Keep your encryption key secure. Anyone with the key can decrypt your data.
- This script uses AES-256 encryption, which is considered secure as of 2024. However, always use up-to-date encryption methods for sensitive data.
- The script does not store encryption keys. You need to remember or securely store your keys separately.

## Contributing

If you find a bug or have a suggestion for improvement, please open an issue or submit a pull request on Codeberg.

## License

This script is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for details.
