<?php

/*
 * Symmetric encryption with custom key (sewck)
 * By PinkOrca
 * Codeberg: https://codeberg.org/PinkOrca/SEWCK
 * Updated with single-line input for decryption
*/

// Function to get multiline input
function getMultilineInput($prompt)
{
    echo $prompt . " (Type 'EOF' on a new line and press Enter to finish):\n";
    $input = "";
    while (($line = fgets(STDIN)) !== false) {
        if (trim($line) === "EOF") {
            break;
        }
        $input .= $line;
    }
    return $input;
}

// Main menu function
function showMenu()
{
    echo "\nWhat do you want to do?\n";
    echo "1. Encrypt\n";
    echo "2. Decrypt\n";
    echo "3. Exit\n";
    return readline("Enter your choice (1, 2, or 3): ");
}

// Main loop
while (true) {
    $operation = showMenu();

    if ($operation == 1) {
        // Encryption
        $plain_text = getMultilineInput("Enter the text to encrypt");
        $key = readline("Enter the encryption key: ");

        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $cipher_text = openssl_encrypt($plain_text, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);
        $encrypted_data = base64_encode($iv . $cipher_text);

        echo "Encrypted data:\n$encrypted_data\n";

        $save_to_file = readline("Do you want to save the encrypted data to a file? (y/n): ");
        if (strtolower($save_to_file) === 'y') {
            $filename = readline("Enter the filename to save: ");
            file_put_contents($filename, $encrypted_data);
            echo "Encrypted data saved to $filename\n";
        }
    } else if ($operation == 2) {
        // Decryption
        $input_method = readline("Do you want to input encrypted data directly (1) or from a file (2)? ");

        if ($input_method == 1) {
            $encrypted_data = readline("Enter the encrypted data: ");
        } else if ($input_method == 2) {
            $filename = readline("Enter the filename containing encrypted data: ");
            $encrypted_data = file_get_contents($filename);
            if ($encrypted_data === false) {
                echo "Error reading file. Please check the filename and try again.\n";
                continue;
            }
        } else {
            echo "Invalid input. Please enter 1 or 2.\n";
            continue;
        }

        $key = readline("Enter the decryption key: ");

        $encrypted_data = base64_decode($encrypted_data);
        $iv_length = openssl_cipher_iv_length('aes-256-cbc');
        $iv = substr($encrypted_data, 0, $iv_length);
        $cipher_text = substr($encrypted_data, $iv_length);

        $plain_text = openssl_decrypt($cipher_text, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);

        echo "Decrypted data:\n$plain_text\n";
    } else if ($operation == 3) {
        // Exit
        echo "Thank you for using SEWCK. Goodbye!\n";
        exit(0);
    } else {
        // Invalid input
        echo "Invalid input. Please enter 1, 2, or 3.\n";
    }

    echo "\n"; // Print a new line before showing the menu again
}
